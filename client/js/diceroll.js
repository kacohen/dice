
function whoopsie(input){
    alert("you need to try a number between 3 and 20, try again!");
}

function rollANumber(input){
    var randNum = Math.floor(Math.random() * input) + 1;
    console.log(randNum);
    return randNum;
}

function setRolledNumber(input){
    var rolledNumber = rollANumber(input);
    $('.dice-display').text(rolledNumber);
    console.log(rolledNumber);
}

function rollIt(input){
    !(input >= 3 && input <=20) ? whoopsie(input) : setRolledNumber(input);
}

