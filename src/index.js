import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore'; // eslint-disable-line import/default
import MainLayout from './components/layouts/MainLayout';
import {loadSettings, loadEngagementSettings} from './actions/settingsActions';
import {loadCustomerSummary} from './actions/customerActions';
import {loadInteractionContext} from './actions/interactionContextActions';
import {loadDealerTeam} from './actions/dealerTeamActions';
import {loadAppointments} from './actions/appointmentActions';
import {loadNotesAndHistory} from './actions/notesAndHistoryActions';
import {initalizeAnalytics} from './utils/googleTagManager';

// The following actions are mock-only and so are disabled for the initial release;
// they will be re-enabled as the true functionality is implemented.
import {loadTasks} from './actions/taskActions';

// import {loadCustomers} from './actions/customerSearchActions';
// import {loadDealers} from './actions/dealerActions';

import './polyfill/find.js';
// import '@coxautokc/fusion-theme/dist/fusion-theme.min.css'; // eslint-disable-line
import './styles/index.scss';

const store = configureStore();
store.dispatch(loadSettings());
store.dispatch(loadEngagementSettings());
store.dispatch(loadCustomerSummary());
store.dispatch(loadInteractionContext());
store.dispatch(loadDealerTeam());
store.dispatch(loadAppointments());
store.dispatch(loadNotesAndHistory({pageNumber: 1, pageSize: 50, notesAndHistoryFilterType: 'SimpleView'}));

// Disabled; see comments above.
store.dispatch(loadTasks());
// store.dispatch(loadCustomers());
// store.dispatch(loadDealers());

initalizeAnalytics('userEvent', 'Vin CRM');

render(
  <Provider store={store}>
    <MainLayout />
  </Provider>,
  document.getElementById('app')
);